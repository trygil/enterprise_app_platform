<?php

use Illuminate\Database\Schema\Blueprint;
use EAP\Migration\Migration;

class Trees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema->create('tree_tmpl_unit', function(Blueprint $t)
        {
            $t->bigIncrements('id');
            $t->integer('lft');
            $t->integer('rgt');
            $t->integer('parent_id')->nullable();
            $t->integer('level')->default(0);
            $t->integer('tmpl_unit_id');
            $t->bigInteger('root_id');
            $t->integer('cluster_id');

            // constraints
            $t->foreign('tmpl_unit_id')->references('id')->on('tmpl_org_unit');
            $t->foreign('cluster_id')->references('id')->on('tmpl_organization');
        });

        $this->schema->create('tree_tmpl_position', function(Blueprint $t)
        {
            $t->bigIncrements('id');
            $t->integer('lft');
            $t->integer('rgt');
            $t->integer('parent_id')->nullable();
            $t->integer('level')->default(0);
            $t->integer('tmpl_position_id');
            $t->bigInteger('root_id');
            $t->integer('cluster_id');

            // constraints
            $t->foreign('tmpl_position_id')->references('id')->on('tmpl_position');
            $t->foreign('cluster_id')->references('id')->on('tmpl_org_unit');
        });

        $this->schema->create('tree_unit', function(Blueprint $t)
        {
            $t->bigIncrements('id');
            $t->integer('lft');
            $t->integer('rgt');
            $t->integer('parent_id')->nullable();
            $t->integer('level')->default(0);
            $t->integer('unit_id');
            $t->bigInteger('root_id');
            $t->integer('cluster_id');

            // constraints
            $t->foreign('unit_id')->references('id')->on('org_unit');
            $t->foreign('cluster_id')->references('id')->on('organization');
        });

        $this->schema->create('tree_position', function(Blueprint $t)
        {
            $t->bigIncrements('id');
            $t->integer('lft');
            $t->integer('rgt');
            $t->integer('parent_id')->nullable();
            $t->integer('level')->default(0);
            $t->integer('position_id');
            $t->bigInteger('root_id');
            $t->integer('cluster_id');

            // constraints
            $t->foreign('position_id')->references('id')->on('position');
            $t->foreign('cluster_id')->references('id')->on('org_unit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schema->dropIfExists('tree_position');
        $this->schema->dropIfExists('tree_unit');
        $this->schema->dropIfExists('tree_tmpl_position');
        $this->schema->dropIfExists('tree_tmpl_unit');
    }
}
