<?php

use Illuminate\Database\Schema\Blueprint;
use EAP\Migration\Migration;

class BusProc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema->create('tmpl_form', function(Blueprint $t)
        {
            $t->increments('id');
            $t->string('form_name', 100);
            $t->string('numbering_rule', 100);
            $t->string('table_name', 100)->nullable();
            $t->string('description', 256);
            $t->timestamps();
        });

        $this->schema->create('tmpl_form_element', function(Blueprint $t)
        {
            $t->string('id', 64);
            $t->integer('ordinal_number')->default(1);
            $t->string('field_name', 64);
            $t->integer('data_type');
            $t->string('caption', 128);
            $t->boolean('is_mandatory')->default(true);
            $t->integer('form_id');
            $t->timestamps();

            // constraints
            $t->primary('id');
            $t->foreign('form_id')->references('id')->on('tmpl_form');
        });

        $this->schema->create('tmpl_attachment', function(Blueprint $t)
        {
            $t->string('id', 32);
            $t->integer('file_type');
            $t->integer('form_id');
            $t->string('form_element_id', 32);
            $t->timestamps();

            // constraints
            $t->primary('id');
            $t->foreign('form_id')->references('id')->on('tmpl_form');
            $t->foreign('form_element_id')->references('id')->on('tmpl_form_element');
        });

        $this->schema->create('tmpl_business_process', function(Blueprint $t)
        {
            $t->string('id', 32);
            $t->integer('step');
            $t->string('description', 128);
            $t->boolean('is_committee')->default(false);
            $t->string('more_action', 128);
            $t->integer('form_id');
            $t->integer('tmpl_position_id')->nullable();
            $t->timestamps();

            // constraints
            $t->primary('id');
            $t->foreign('form_id')->references('id')->on('tmpl_form');
            $t->foreign('tmpl_position_id')->references('id')->on('tmpl_position');
        });

        $this->schema->create('tmpl_committee', function(Blueprint $t)
        {
            $t->string('id', 32);
            $t->string('committee_name', 64);
            $t->string('description', 256);
            $t->string('tmpl_busproc_id', 32);
            $t->timestamps();

            // constraints
            $t->primary('id');
            $t->foreign('tmpl_busproc_id')->references('id')->on('tmpl_business_process');
        });

        $this->schema->create('tmpl_committee_position', function(Blueprint $t)
        {
            $t->string('tmpl_committee_id', 32);
            $t->integer('tmpl_position_id');
            $t->integer('flag')->default(1);
            $t->timestamps();

            // Constaints
            $t->foreign('tmpl_committee_id')->references('id')->on('tmpl_committee');
            $t->foreign('tmpl_position_id')->references('id')->on('tmpl_position');
        });

        $this->schema->create('document', function(Blueprint $t)
        {
            $t->string('id', 32);
            $t->string('document_number', 32);
            $t->text('content')->nullable();
            $t->integer('status')->default(0);
            $t->integer('tmpl_form_id');
            $t->bigInteger('creator');
            $t->timestamps();

            // constraints
            $t->primary('id');
            $t->foreign('tmpl_form_id')->references('id')->on('tmpl_form');
            $t->foreign('creator')->references('id')->on('personnel');
        });

        $this->schema->create('attachment', function(Blueprint $t)
        {
            $t->string('filepath', 512);
            $t->string('physical_name', 256);
            $t->string('document_id', 32);
            $t->string('tmpl_attachment_id', 32);
            $t->timestamps();

            // constraints
            $t->foreign('document_id')->references('id')->on('document');
            $t->foreign('tmpl_attachment_id')->references('id')->on('tmpl_attachment');
        });

        $this->schema->create('business_process', function(Blueprint $t)
        {
            $t->string('id', 64);
            $t->smallInteger('execution_status')->default(0);
            $t->string('document_id', 32);
            $t->dateTime('start_date');
            $t->dateTime('finish_date')->nullable();
            $t->string('comment', 256);
            $t->bigInteger('executor');
            $t->string('tmpl_busproc_id', 32);
            $t->timestamps();

            // constraints
            $t->primary('id');
            $t->foreign('document_id')->references('id')->on('document');
            $t->foreign('executor')->references('id')->on('position');
            $t->foreign('tmpl_busproc_id')->references('id')->on('tmpl_business_process');            
        });

        $this->schema->create('committee', function(Blueprint $t)
        {
            $t->string('id', 32);
            $t->integer('approval_status')->default(0); // Sum of position flag from tmpl_committee
            $t->integer('date_approved')->nullable();
            $t->string('tmpl_committee_id', 32);
            $t->string('business_process_id', 32);

            // constraints
            $t->primary('id');
            $t->foreign('tmpl_committee_id')->references('id')->on('tmpl_committee');
            $t->foreign('business_process_id')->references('id')->on('business_process');
        });

        $this->schema->create('committee_position', function(Blueprint $t)
        {
            $t->string('committee_id', 32);
            $t->integer('position_id');
            $t->integer('flag')->default(1);
            $t->timestamps();

            // Constaints
            $t->foreign('committee_id')->references('id')->on('committee');
            $t->foreign('position_id')->references('id')->on('position');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schema->dropIfExists('committee_position');
        $this->schema->dropIfExists('committee');
        $this->schema->dropIfExists('business_process');
        $this->schema->dropIfExists('attachment');
        $this->schema->dropIfExists('document');
        $this->schema->dropIfExists('tmpl_committee_position');
        $this->schema->dropIfExists('tmpl_committee');
        $this->schema->dropIfExists('tmpl_business_process');
        $this->schema->dropIfExists('tmpl_attachment');
        $this->schema->dropIfExists('tmpl_form_element');
        $this->schema->dropIfExists('tmpl_form');
    }
}
