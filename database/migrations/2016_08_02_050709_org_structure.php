<?php

use Illuminate\Database\Schema\Blueprint;
use EAP\Migration\Migration;

class OrgStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema->create('tmpl_organization', function(Blueprint $t)
        {
            $t->increments('id');
            $t->integer('level');
            $t->string('org_name', 100);
            $t->timestamps();
        });

        $this->schema->create('tmpl_org_unit', function(Blueprint $t)
        {
            $t->increments('id');
            $t->string('unit_name', 100);
            $t->smallInteger('unit_kind')->default(0);
            $t->string('mission_statement', 256);
            $t->integer('tmpl_org_id');
            $t->timestamps();

            // Contraint
            $t->foreign('tmpl_org_id')->references('id')->on('tmpl_organization');
        });

        $this->schema->create('tmpl_position', function(Blueprint $t)
        {
            $t->increments('id');
            $t->string('position_name', 100);
            $t->smallInteger('position_type')->default(0);
            $t->string('qualification', 256);
            $t->string('jobdesc', 256);
            $t->integer('tmpl_unit_id'); 
            $t->timestamps();

            // Contraint
            $t->foreign('tmpl_unit_id')->references('id')->on('tmpl_org_unit');
        });

        $this->schema->create('territory', function(Blueprint $t)
        {
            $t->increments('id');
            $t->integer('level');
            $t->string('area_name', 100);
            $t->timestamps();
        });

        $this->schema->create('organization', function(Blueprint $t)
        {
            $t->increments('id');
            $t->string('org_name', 100);
            $t->integer('territory_id');
            $t->integer('tmpl_org_id');
            $t->boolean('is_hq')->default(false);
            $t->timestamps();

            // constraints
            $t->foreign('territory_id')->references('id')->on('territory');
            $t->foreign('tmpl_org_id')->references('id')->on('tmpl_organization');
        });

        $this->schema->create('org_unit', function(Blueprint $t)
        {
            $t->increments('id');
            $t->string('unit_name', 100);
            $t->integer('org_id');
            $t->integer('tmpl_org_unit_id');
            $t->timestamps();

            // constraints
            $t->foreign('org_id')->references('id')->on('organization');
            $t->foreign('tmpl_org_unit_id')->references('id')->on('tmpl_org_unit');
        });

        $this->schema->create('position', function(Blueprint $t)
        {
            $t->increments('id');
            $t->string('position_name', 100);
            $t->integer('unit_id');
            $t->integer('tmpl_position_id');
            $t->timestamps();

            // constraints
            $t->foreign('unit_id')->references('id')->on('org_unit');
            $t->foreign('tmpl_position_id')->references('id')->on('tmpl_position');
        });

        $this->schema->create('personnel', function(Blueprint $t)
        {
            $t->bigIncrements('id');
            $t->string('full_name', 256);
            $t->string('id_number', 64);
            $t->string('email', 256);
            $t->bigInteger('user_id'); // relation need to create manually
            $t->integer('org_id');
            $t->integer('unit_id');
            $t->integer('position_id');
            $t->timestamps();

            // constraints
            $t->foreign('org_id')->references('id')->on('organization');
            $t->foreign('unit_id')->references('id')->on('org_unit');
            $t->foreign('position_id')->references('id')->on('position');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schema->dropIfExists('personnel');
        $this->schema->dropIfExists('position');
        $this->schema->dropIfExists('org_unit');
        $this->schema->dropIfExists('organization');
        $this->schema->dropIfExists('territory');
        $this->schema->dropIfExists('tmpl_position');
        $this->schema->dropIfExists('tmpl_org_unit');
        $this->schema->dropIfExists('tmpl_organization');
    }
}
