<?php 
namespace EAP\Models\Struct;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
	protected $table      = "unit";
	protected $primaryKey = "id";

	public function organization()
	{
		return $this->hasOne('EAP\Models\Struct\Organization', 'org_id');
	}

	public function positions()
	{
		return $this->hasMany('EAP\Models\Struct\Position', 'unit_id', 'id');
	}

	public function template()
	{
		return $this->belongsTo('EAP\Models\Struct\TUnit', 'tmpl_org_unit_id', 'id');
	}
}
