<?php 
namespace EAP\Models\Struct;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
	protected $table      = "position";
	protected $primaryKey = "id";

	public function personnel()
	{
		return $this->hasOne('EAP\Models\Struct\Personnel', 'position_id', 'id');
	}

	public function unit()
	{
		return $this->belongsTo('EAP\Models\Struct\Unit', 'unit_id', 'id');
	}

	public function personnel()
	{
		return $this->hasMany('EAP\Models\Struct\Personnel', 'position_id', 'id');
	}
}
