<?php 
namespace EAP\Models\Struct;

use Illuminate\Database\Eloquent\Model;

class Territory extends Model
{
	protected $table      = "territory";
	protected $primaryKey = "id";

	public function organizations()
	{
		return $this->hasMany('EAP\Models\Struct\Organizations', 'territory_id', 'id');
	}
}
