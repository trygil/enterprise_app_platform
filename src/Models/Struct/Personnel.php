<?php
namespace EAP\Models\Struct;

use Illuminate\Database\Eloquent\Model;

class Personnel extends Model
{
	protected $table      = "personnel";
	protected $primaryKey = "id";

	public function position()
	{
		return $this->belongsTo('EAP\Models\Struct\Position', 'position_id');
	}

	public function organization()
	{
		return $this->belongsTo('EAP\Models\Struct\Organization', 'org_id');
	}

	public function unit()
	{
		return $this->belongsTo('EAP\Models\Struct\Unit', 'unit_id');
	}
}
