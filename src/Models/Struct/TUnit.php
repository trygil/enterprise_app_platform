<?php 
namespace EAP\Models\Struct;

use Illuminate\Database\Eloquent\Model;

class TUnit extends Model
{
	protected $table      = "tmpl_org_unit";
	protected $primaryKey = "id";

	public function instances()
	{
		return $this->hasMany('EAP\Models\Struct\Unit', 'tmpl_org_unit_id', 'id');
	}

	public function tPositions()
	{
		return $this->hasMany('EAP\Models\Struct\TPosition', 'tmpl_unit_id', 'id');
	}
}
