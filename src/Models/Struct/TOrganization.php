<?php 
namespace EAP\Models\Struct;

use Illuminate\Database\Eloquent\Model;

class TOrganization extends Model
{
	protected $table      = "tmpl_organization";
	protected $primaryKey = "id";

	public function instances()
	{
		return $this->hasMany('EAP\Models\Struct\Organization', 'tmpl_org_id', 'id');
	}

	public function tUnits()
	{
		return $this->hasMany('EAP\Models\Struct\TUnit', 'tmpl_org_id', 'id');
	}
}
