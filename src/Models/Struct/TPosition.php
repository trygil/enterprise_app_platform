<?php 
namespace EAP\Models\Struct;

use Illuminate\Database\Eloquent\Model;

class TPosition extends Model
{
	protected $table      = "tmpl_position";
	protected $primaryKey = "id";

	public function instances()
	{
		return $this->hasMany('EAP\Models\Struct\Position', 'tmpl_position_id', 'id');
	}

	public function tUnit()
	{
		return $this->belongsTo('EAP\Models\Struct\TUnit', 'tmpl_unit_id');
	}
}
