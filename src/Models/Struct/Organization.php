<?php 
namespace EAP\Models\Struct;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
	protected $table      = "organization";
	protected $primaryKey = "id";

	public function isHQ()
	{
		return $this->is_hq;
	}

	// ============== Relations ==============

	public function territory()
	{
		return $this->hasOne('EAP\Models\Struct\Territory', 'territory_id');
	}

	public function units()
	{
		return $this->hasMany('EAP\Models\Struct\Unit', 'org_id', 'id');
	}

	public function template()
	{
		return $this->belongsTo('EAP\Models\Struct\TOrganization', 'tmpl_org_id', 'id');
	}
}