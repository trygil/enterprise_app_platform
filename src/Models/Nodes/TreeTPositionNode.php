<?php 
namespace EAP\Models\Nodes;

use Illuminate\Database\Eloquent\Model;

class TreeTPositionNode extends Model
{
	protected $table      = "tree_tmpl_position";
	protected $primaryKey = "id";

	public function model()
	{
		return $this->hasOne('EAP\Models\Struct\TPosition', 'tmpl_position_id', 'id');
	}

	public function parent()
	{
		return $this->belongsTo('EAP\Models\Nodes\TreeTPositionNode', 'parent_id', 'id');
	}

	public function children()
	{
		return $this->hasMany('EAP\Models\Nodes\TreeTPositionNode', 'parent_id', 'id');
	}
}