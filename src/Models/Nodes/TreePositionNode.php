<?php 
namespace EAP\Models\Nodes;

use Illuminate\Database\Eloquent\Model;

class TreePositionNode extends Model
{
	protected $table      = "tree_position";
	protected $primaryKey = "id";

	public function model()
	{
		return $this->hasOne('EAP\Models\Struct\Position', 'position_id', 'id');
	}

	public function parent()
	{
		return $this->belongsTo('EAP\Models\Nodes\TreePositionNode', 'parent_id', 'id');
	}

	public function children()
	{
		return $this->hasMany('EAP\Models\Nodes\TreePositionNode', 'parent_id', 'id');
	}
}