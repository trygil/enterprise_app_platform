<?php 
namespace EAP\Models\Nodes;

use Illuminate\Database\Eloquent\Model;

class TreeUnitNode extends Model
{
	protected $table      = "tree_unit";
	protected $primaryKey = "id";

	public function model()
	{
		return $this->hasOne('EAP\Models\Struct\Unit', 'unit_id', 'id');
	}

	public function parent()
	{
		return $this->belongsTo('EAP\Models\Nodes\TreeUnitNode', 'parent_id', 'id');
	}

	public function children()
	{
		return $this->hasMany('EAP\Models\Nodes\TreeUnitNode', 'parent_id', 'id');
	}
}