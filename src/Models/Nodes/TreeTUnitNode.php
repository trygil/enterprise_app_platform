<?php 
namespace EAP\Models\Nodes;

use Illuminate\Database\Eloquent\Model;

class TreeTUnitNode extends Model
{
	protected $table      = "tree_tmpl_unit";
	protected $primaryKey = "id";

	public function model()
	{
		return $this->hasOne('EAP\Models\Struct\TUnit', 'tmpl_unit_id', 'id');
	}

	public function parent()
	{
		return $this->belongsTo('EAP\Models\Nodes\TreeTUnitNode', 'parent_id', 'id');
	}

	public function children()
	{
		return $this->hasMany('EAP\Models\Nodes\TreeTUnitNode', 'parent_id', 'id');
	}
}