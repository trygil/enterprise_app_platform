<?php 
namespace EAP\Models\Process;

use Illuminate\Database\Eloquent\Model;

class TBusinessProcess extends Model
{
	protected $table      = "tmpl_business_process";
	protected $primaryKey = "id";

	public function form()
	{
		return $this->belongsTo('EAP\Models\Process\TForm', 'form_id', 'id');
	}

	public function tPosition()
	{
		return $this->hasMany('EAP\Models\Struct\TPosition', 'tmpl_position_id', 'id');
	}

	public function save()
	{
		$this->id = $this->generateID();

		parent::save();
	}

	private generateID()
	{
		$id = date('YmdHis');
		$id = $this->form_id . str_replace('=', '', base64_encode($id));

		return $id;
	}
}
