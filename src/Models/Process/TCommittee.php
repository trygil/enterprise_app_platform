<?php 
namespace EAP\Models\Process;

use Illuminate\Database\Eloquent\Model;

class TCommittee extends Model
{
	protected $table      = "tmpl_committee";
	protected $primaryKey = "id";

	public function tBusinessProcess()
	{
		return $this->belongsTo('EAP\Models\Process\TBusinessProcess', 'tmpl_busproc_id', 'id');
	}

	public function tPositions()
	{
		return $this->belongsToMany('EAP\Models\Struct\TPosition', 
									'tmpl_committee_position', 
									'tmpl_committee_id', 
									'tmpl_position_id');
	}

	public function save()
	{
		$this->id = $this->generateID();

		parent::save();
	}

	private generateID()
	{
		$id = date('YmdHis');
		$id = $this->tmpl_busproc_id . str_replace('=', '', base64_encode($id));

		return $id;
	}
}
