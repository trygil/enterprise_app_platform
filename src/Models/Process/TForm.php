<?php 
namespace EAP\Models\Process;

use Illuminate\Database\Eloquent\Model;

class TForm extends Model
{
	protected $table      = "tmpl_form";
	protected $primaryKey = "id";

	public function instances()
	{
		return $this->hasMany('EAP\Models\Process\Document', 'tmpl_form_id', 'id');
	}

	public function elements()
	{
		return $this->hasMany('EAP\Models\Process\TFormElement', 'form_id', 'id');
	}

	public function attachments()
	{
		return $this->hasMany('EAP\Models\Process\TAttachment', 'form_id', 'id');
	}

	public function tBusinessProcesses()
	{
		return $this->hasMany('EAP\Models\Process\TBusinessProcess', 'form_id', 'id')
					->orderBY('step');
	}

	public function save()
	{
		$this->id = $this->generateID();

		parent::save();
	}

	private generateID()
	{
		$id = date('YmdHis');
		$id = str_replace('=', '', base64_encode($id));

		return $id;
	}
}
