<?php 
namespace EAP\Models\Process;

use Illuminate\Database\Eloquent\Model;

class BusinessProcess extends Model
{
	const STATUS_PENDING  = 0;
	const STATUS_APPROVED = 1;
	const STATUS_REJECTED = 2;

	protected $table      = "business_process";
	protected $primaryKey = "id";

	public function document()
	{
		return $this->belongsTo('EAP\Models\Process\Document', 'document_id', 'id');
	}

	public function executor()
	{
		return $this->hasOne('EAP\Models\Struct\Position', 'executor', 'id')
	}

	public function committee()
	{
		return $this->belongsTo('EAP\Models\Struct\Committee', 'business_process_id', 'id');
	}

	public function template()
	{
		return $this->belongsTo('EAP\Models\Struct\TBusinessProcess', 'tmpl_busproc_id', 'id');
	}

	public function save()
	{
		$this->id = $this->generateID();

		parent::save();
	}

	private generateID()
	{
		$id = $this->document->id;
		$id = $this->template->id . str_replace('=', '', base64_encode($id));

		return $id;
	}
}
