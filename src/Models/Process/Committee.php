<?php 
namespace EAP\Models\Process;

use Illuminate\Database\Eloquent\Model;

class Committee extends Model
{
	protected $table      = "committee";
	protected $primaryKey = "id";

	public function businessProcess()
	{
		return $this->belongsTo('EAP\Models\Process\BusinessProcess', 'business_process_id', 'id');
	}

	public function positions()
	{
		return $this->belongsToMany('EAP\Models\Struct\TPosition', 
									'committee_position', 
									'committee_id', 
									'position_id')
					->withPivot('flag');
	}

	public function save()
	{
		$this->id = $this->generateID();

		parent::save();
	}

	private generateID()
	{
		$id = $this->business_process_id;
		$id = $this->tmpl_committee_id . str_replace('=', '', base64_encode($id));

		return $id;
	}
}
