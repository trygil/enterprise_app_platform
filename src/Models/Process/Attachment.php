<?php 
namespace EAP\Models\Process;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
	protected $table      = "attachment";
	protected $primaryKey = "id";

	public function document()
	{
		return $this->belongsTo('EAP\Models\Process\Document', 'document_id', 'id');
	}

	public function template()
	{
		return $this->belongsTo('EAP\Models\Process\TAttachment', 'tmpl_attachment_id', 'id');
	}
}
