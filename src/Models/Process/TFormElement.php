<?php 
namespace EAP\Models\Process;

use Illuminate\Database\Eloquent\Model;

class TFormElement extends Model
{
	protected $table      = "tmpl_form_element";
	protected $primaryKey = "id";

	public function form()
	{
		return $this->belongsTo('EAP\Models\Process\TForm', 'form_id');
	}
}


