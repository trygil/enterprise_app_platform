<?php 
namespace EAP\Models\Process;

use Illuminate\Database\Eloquent\Model;

class TAttachment extends Model
{
	protected $table      = "tmpl_attachment";
	protected $primaryKey = "id";

	public function form()
	{
		return $this->belongsTo('EAP\Models\Process\TForm', 'form_id', 'id');
	}

	public function formElement()
	{
		return $this->hasMany('EAP\Models\Process\TFormElement', 'form_element_id', 'id');
	}

	public function save()
	{
		$this->id = $this->generateID();

		parent::save();
	}

	private generateID()
	{
		$id = date('YmdHis');
		$id = $this->form->id . str_replace('=', '', base64_encode($id));

		return $id;
	}
}
