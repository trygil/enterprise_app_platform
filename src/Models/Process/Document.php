<?php 
namespace EAP\Models\Process;

use Illuminate\Database\Eloquent\Model;
use EAP\Models\Process\BusinessProcess;

class Document extends Model
{
	const STATUS_DRAFT    = 0;
	const STATUS_PENDING  = 1;
	const STATUS_APPROVE  = 2;
	const STATUS_REJECT   = 3;
	const STATUS_REVISION = 4;

	protected $table      = "document";
	protected $primaryKey = "id";

	public function attachments()
	{
		return $this->hasMany('EAP\Models\Process\Attachment', 'document_id', 'id');
	}

	public function creator()
	{
		return $this->hasOne('EAP\Models\Struct\Personnel', 'creator');
	}

	public function businessProcesses()
	{
		return $this->hasMany('EAP\Models\Process\BusinessProcess', 'document_id', 'id');
	}

	public function currentBusinessProcess()
	{
		return BusinessProcess::where('document_id', '=', $this->id)
								->where('execution_status', '=', BusinessProcess::STATUS_PENDING)
								->first();
	}

	public function template()
	{
		return $this->belongsTo('EAP\Models\Process\TForm', 'tmpl_form_id', 'id');
	}

	public function save()
	{
		$this->id = $this->generateID();

		parent::save();
	}

	private generateID()
	{
		$id = $this->creator->id;
		$id = $this->template->id . str_replace('=', '', base64_encode($id));

		return $id;
	}
}
