<?php

use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Cloner\VarCloner;

if (! function_exists('deb'))
{
	function deb()
	{
		$traces = [];
		foreach (debug_backtrace() as $d)
			if (isset($d['file']))
				$traces[] = $d['file'] .' '. $d['line'];

		if (count($traces) > 5)
			$traces = array_splice($traces, 0, 5);

		$debug = array_merge($traces, func_get_args());

		$dumper = 'cli' === PHP_SAPI ? new CliDumper : new VarCloner;

		array_map(function($x) use($dumper) { ($dumper->dump((new VarCloner)->cloneVar($x))); }, $debug);

		die;
	}
}


if (! function_exists('is'))
{
	function is($vals, $search)
	{
		return ($vals & $search) == $search;
	}
}

if (! function_exists('str_random'))
{
	function str_random($length = 16)
	{
		$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

		return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
	}
}