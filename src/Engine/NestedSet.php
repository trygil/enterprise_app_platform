<?php

namespace EAP\Engine;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Model;

abstract class NestedSet
{
	protected $current_node = null;
	protected $node_class   = null;
	private   $table_name   = null;

	const EXC_SELF_NODE = 0x01;
	const EXC_ROOT_NODE = 0x02;
	const CHILD_FIRST   = 0x04;

	/**
	 * create root node with the given model as its data.
	 * @param  Model  	$model 	model associate with the node.
	 * @return integer          inserted id of node.
	 */
	public function createRoot(Model $model, $change_curr_node = true)
	{
		$this->table_name = $model->getTable();

		$class = $this->getModelClassName();
		
		// create node model
		$node_model             = new $class;
		$node_model->lft        = 1;
		$node_model->rgt        = 2;
		$node_model->parent_id  = 0;
		$node_model->level      = 0;
		$node_model->cluster_id = $this->getClusterID();
		$node_model->created_at = date('Y-m-d H:i:s');
		$node_model->model      = $model;

		// insert into database
		$node_model->save();
		
		// update root_id
		$node_model->root_id = $node_model->id;
		$node_model->save();

		if ($change_curr_node)
			$this->current_node = $node_model;

		return $root_id;
	}

	/**
	 * insert nodes from given JSON structure.
	 * @param [array|string]  $json         nodes in form of JSON string or array associative.
	 * @param Model 		  $target_node
	 */
	public function addNodesFromJson($json, Model $target_node = null)
	{
		$json     = is_string($json) ? json_decode($json, true) : (array) $json;
		$trees    = [];

		if (is_a($target_node, $this->getModelClassName()))
			$node_id = $target_node->id;
		elseif ($this->getNode($target_node))
			$node_id = $this->getNode($target_node, false)->id;

		/**
		 * $jsonToNestedSet
		 * convert given JSON into nested set.
		 * @param $_nodes  JSON
		 */
		$jsonToNestedSet = function ($_nodes, $left  = 1) 
			use(& $trees, & $jsonToNestedSet)
		{
			static $ri = 0;
			$right = 1;
			$i = 0;
			foreach ($_nodes as $node)
			{
				$temp = $node;

				unset($temp['on_edit']);
				unset($temp['children']);

				if ($node['level'] == 0)
					$ri = $i;

				$trees[$ri][] = $temp;
				$c = count($trees[$ri]) - 1;

				// If node has older siblings and not root.
				if ($i > 0 && $node['level'] != 0)
					$trees[$ri][$c]['lft'] = $left = $right + 1;
				// First child of its parent
				else
					$trees[$ri][$c]['lft'] = $left;

				// Collect root(s) index
				if ($node['level'] == 0)
					$roots[] = $c;

				// If has children
				if (count($node['children']) > 0)
					$trees[$ri][$c]['rgt'] = $right = $jsonToNestedSet($node['children'], $left + 1) + 1;
				// Lowest node in a branch
				else
					$trees[$ri][$c]['rgt'] = $right = $left + 1;

				$i++;
			}

			return $right;
		};

		// Convert json into nested set
		$jsonToNestedSet($json);

		// Inseruto into databesu
		Capsule::transaction(function()
		{
			foreach ($trees as $ti => $nodes)
			{
				foreach ($nodes as $ni => $node)
				{
					static $root_id      = 1;
					static $level_parent = [];

					$is_saved  = $node['saved']; unset($node['saved']);
					$is_edited = $node['edited']; unset($node['edited']);

					// root
					if ($ni == 0)
					{
						if ($is_saved)
						{
							$level_parent[0] = $root_id = $node['id'];

							if ($is_edited) // edit root
							{
								$node_id = $node['id'];
								unset($node['id']);

								// update record
								$qry = Capsule::table($this->table_name);
								$qry->where('id', '=', $node_id)
									->update($node);
							}
						}

						else // create new root
						{
							$qry = Capsule::table($this->table_name);
							$level_parent[0] = $root_id = $qry->insertGetId($node);
						}
					}
					
					// descendants 
					else
					{
						$parent_id = $level_parent[$node['level'] - 1];

						$node['parent_id'] = $parent_id;
						$node['root_id']   = $root_id;

						$node_id = $node['id'];

						if ($is_saved)
						{
							if ($is_edited) // edit node
							{
								unset($node['id']);

								// update record
								$qry = Capsule::table($this->table_name);
								$qry->where('id', '=', $node_id)
									->update($node);
							}
						}

						else // insert into database
							$node_id = Capsule::table($this->table_name)->insertGetId($node);

						// if not the lowest node
						if ($node['rgt'] - $node['lft'] > 1)
							$level_parent[$node['level']] = $node_id;
					}
				}
			}	
		});

		return true;
	}

	public function getRoots($cluster_id = null)
	{
		$query = Capsule::table($this->table_name);
		$query->select("*")->where('parent_id', '=', 0);

		if ($cluster_id != null)
			$query->where('cluster_id', '=', $cluster_id);

		return $query->get();
	}

	public function getRoot()
	{
		if ($this->current_node == null)
			throw new \Exception('NULL node pointer exception. \'getNode()\' first before do this operation.');

		$node = $this->current_node;

		$qry = Capsule::table($this->table_name);
		$qry->select("*")
			->where('lft', '=', 1)
			->where('root_id', '=', $node['root_id']);

		return $qry->get();
	}

	public function getNode($node_id = null, $change_curr_node = true)
	{
		$node = $this->current_node;

		// update current node.
		if ($node_id != null)
		{
			$node = ($this->node_class)::find($node_id);

			if ($change_curr_node)
			{
				$this->current_node = $node;
				$this->table_name   = $this->current_node->model->getTable();
			}
		}

		return $node;
	}

	public function getAncestors($mode = self::EXC_SELF_NODE)
	{
		if ($this->current_node == null)
			throw new \Exception('NULL node pointer exception. \'getNode()\' first before do this operation.');

		$node   = $this->current_node;
		$params = [$node['lft'], $node['rgt'], $node['root_id']];

		$qry = Capsule::table($this->table_name);
		$qry->select("*")
			->where('root_id', '=', $node->root_id)
			->orderBy('lft', is($mode, self::CHILD_FIRST) ? 'desc' : 'asc');
		
		$exc_self = is($mode, self::EXC_SELF_NODE);
		
		$qry->where('lft', $exc_self ? '<' : '<=', $node->lft)
			->where('rgt', $exc_self ? '>' : '>=', $node->rgt);

		if (is($mode, self::EXC_ROOT_NODE))
			$qry->where('parent_id', '<>', 0);
		
		return $qry->get();
	}

	public function getDescendants($node_id = null, $start_level = 1, $levels = null)
	{
		$start_level = $start_level < 1 ? 1 : $start_level;

		if ($this->current_node == null && $node_id == null)
			throw new \Exception('NULL node pointer exception. \'getNode()\' first before do this operation.');

		$class = $this->getModelClassName();
		$node  = $this->current_node;

		if ($node_id != null)
			$node = $this->getNode($node_id);

		$qry = ($class)::where('lft', '>=', $node->lft);
		$qry->where('rgt', '<=', $node->rgt);

		// Fetch data from specified level
		if ($start_level != 1)
			$qry->where('level', '>=', $start_level);

		if ($levels != null)
			$qry->where('level', '<', $levels);

		return $qry->get();
	}

	public function getChildren($node_id = null)
	{
		if ($this->current_node == null && $node_id == null)
			throw new \Exception('NULL node pointer exception. \'getNode()\' first before do this operation.');

		$class = $this->getModelClassName();
		$node = $this->current_node;

		if ($node_id != null)
			$node = $this->getNode($node_id);

		return $node->children;
	}

	/**
	 * return list of nodes by 'root_id'.
	 * @param  int    $root_id      tree identifier
	 * @param  array  $restriction  [optional] additional query restriction.
	 * @return array                tree nodes
	 */
	public function getTreeNodesByRoot($root_id, $restriction = [])
	{
		$class = $this->getModelClassName();
		$qry = ($class)::where('root_id', '=', $root_id);
		$qry->orderBy('level')
			->orderBy('lft');

		return $qry->get();
	}

	/**
	 * return list of nodes by 'cluster_id'.
	 * @param  int    $cluster_id   cluster identifier
	 * @param  array  $restriction  [optional] additional query restriction.
	 * @return array                tree nodes
	 */
	public function getTreeNodesByCluster($cluster_id, $restriction = [])
	{
		$class = $this->getModelClassName();
		$qry = ($class)::where('cluster_id', '=', $cluster_id);
		$qry->orderBy('root_id')
			->orderBy('level')
			->orderBy('lft');

		return $qry->get();
	}

	/**
	 * add child node to current node or specified node.
	 * @param Model $child_node      child node object
	 * @param Model $target_node     [optional] target node object
	 */
	public function addNode(Model $child_node, Model $target_node = null)
	{
		$class = $this->getModelClassName();
		if ($this->current_node == null && !is_a($target_node, $class))
			throw new \Exception('NULL node pointer exception. \'getNode()\' first before do this operation.');		

		if ($target_node == null)
			$target_node = $this->current_node;

		// Set root_id & parent_id.
		$child_node->root_id   = $target_node->root_id;
		$child_node->parent_id = $target_node->id;

		$is_success = Capsule::transaction(function () use($target_node, $child_node)
		{
			// Insert new node
			$child_node->lft = $target_node->rgt;
			$child_node->rgt = $target_node->rgt + 1;
			$child_node->save();

			# Re-arrange existing node(s)
			$qry = Capsule::table($target_node->getTable());
			$qry->where('root_id', '=', $target_node->root_id)
				->where('lft', '>=', $target_node->rgt)
				->update(['lft' => Capsule::raw('lft + 2')]); // Shift left values

			$qry = Capsule::table($target_node->getTable());
			$qry->where('root_id', '=', $target_node->root_id)
				->where('rgt', '>=', $target_node->rgt)
				->update(['rgt' => Capsule::raw('rgt + 2')]); // Shift right values
		});

		return $is_success;
	}

	################### Abstract Methods ###################
	abstract protected function getClusterID(); // return cluster id
	abstract protected function getModelClassName(); // return node model class name
}
