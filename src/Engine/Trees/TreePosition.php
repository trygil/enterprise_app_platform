<?php

namespace EAP\Engine\Trees;

use EAP\Engine\NestedSet;

class TreePosition extends NestedSet
{
	protected $cluster_id = 'unit_id';

	protected function getClusterID()
	{
		return $this->model->{$this->cluster_id};
	}

	protected function getModelClassName()
	{
		return 'EAP\Models\Nodes\TreePositionNode';
	}
}