<?php

namespace EAP\Engine\Trees;

use EAP\Engine\NestedSet;

class TreeTemplatePosition extends NestedSet
{
	protected $cluster_id = 'tmpl_unit_id';

	protected function getClusterID()
	{
		return $this->model->{$this->cluster_id};
	}

	protected function getModelClassName()
	{
		return 'EAP\Models\Nodes\TreeTPositionNode';
	}
}