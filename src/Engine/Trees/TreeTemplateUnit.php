<?php

namespace EAP\Engine\Trees;

use EAP\Engine\NestedSet;

class TreeTemplateUnit extends NestedSet
{
	protected $cluster_id = 'tmpl_org_id';

	protected function getClusterID()
	{
		return $this->model->{$this->cluster_id};
	}

	protected function getModelClassName()
	{
		return 'EAP\Models\Nodes\TreeTUnitNode';
	}
}