<?php

namespace EAP\Engine;

use Illuminate\Database\Eloquent\Model;
use EAP\Engine\Trees;
use EAP\Models\Process\TForm;
use EAP\Models\Struct\Pesonnel;
use EAP\Models\Struct\Position;
use EAP\Models\Process\Document;
use EAP\Models\Process\BusinessProcess;

class Workflow
{
	private $document;
	private $tmpl_bus_procs;
	private $initiator;

	public function __construct(Model $model)
	{
		if ($model instanceof TForm)
		{
			$this->initDocument($model);
		}
		else
			$this->document = $model;

		$tmpl_form = $this->document->template;
		$this->tmpl_bus_procs = $tmpl_form->tBusinessProcesses;
	}

	public function initDocument(TForm $model, array $data, Pesonnel $initiator)
	{
		$document = new Document;
		$document->template = $model;
		$document->content  = json_encode($data);
		$document->status   = Document::STATUS_DRAFT;
		$document->creator  = $initiator;
		$document->document_number = '';
		$document->save();

		$this->document = $document;

		return $document->id;
	}

	public function startProcess()
	{
		$curr_busproc = $this->document->currentBusinessProcess();
		if ($curr_busproc != null)
			throw \Exception('Document already intiated.');

		$this->createProcess(1);
	}

	public function createProcess($step)
	{
		$tbusproc = $this->tmpl_bus_procs[$step];

		$busproc = new BusinessProcess;
		$busproc->execution_status = BusinessProcess::STATUS_PENDING;
		$busproc->document         = $this->document;
		$busproc->start_date       = date('Y-m-d H:i:s');
		$busproc->finish_date      = null;
		$busproc->comment          = "";
		$busproc->executor         = null;
		$busproc->template         = $tbusproc;
	}

	public function approve(Personel $person)
	{
		$this->changeBusProcStatus($person, BusinessProcess::STATUS_APPROVED)

		$busproc = $this->document->currentBusinessProcess();

		if (count($this->tmpl_bus_procs) > $busproc->step + 1 && $busproc)
		{
			$tbusproc = $this->tmpl_bus_procs;
			$this->createProcess($busproc->step + 1);
		}
	}

	public function reject(Personnel $person)
	{
		$this->changeBusProcStatus($person, BusinessProcess::STATUS_REJECTED)
	}

	private function changeBusProcStatus(Personnel $person, $status)
	{
		$busproc  = $this->document->currentBusinessProcess();
		$template = $busproc->template;

		if ($template->is_committee)
		{
			$flag = 0;

			foreach ($busproc->committee->positions as $position)
			{
				static $i = 0;

				if ($person->position->id == $position->id)
				{
					if ($status == BusinessProcess::STATUS_APPROVED)
						$position->pivot->flag = pow(2, ++$i);

					$position->save();
				}

				$flag += $position->pivot->flag;
			}

			if ($flag == pow(2, count($busproc->committee->positions)) - 1)
			{
				$busproc->status = $status;
				$busproc->save();
			}
		}
		else
		{
			$busproc->status = $status;
			$busproc->save();
		}

		return $busproc;
	}
}
