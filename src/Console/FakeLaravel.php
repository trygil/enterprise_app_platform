<?php

namespace EAP\Console;

use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Input\ArrayInput;

class FakeLaravel {

	private $command;
	private $basePath;
	private $databasePath;

	public function __construct($command)
	{
		$this->command = $command;
	}

	/**
	 * Run an Artisan console command by name.
	 *
	 * @param  string  $command
	 * @param  array  $parameters
	 * @return int
	 */
	public function call($command, array $parameters = array())
	{
		$parameters['command'] = $command;
		$this->lastOutput = new BufferedOutput;

		if (method_exists($this->command , 'fire'))
			return $this->command->fire(new ArrayInput($parameters), $this->lastOutput);

		else if (method_exists($this->command , 'handle'))
			return $this->command->handle(new ArrayInput($parameters), $this->lastOutput);

		else
			$this->command->error('missing "fire" and "handle" ');
	}

	/**
	 * Get the base path of the Laravel installation.
	 *
	 * @return string
	 */
	public function basePath()
	{
		return $this->basePath;
	}

	/**
	 * Get the path to the application configuration files.
	 *
	 * @return string
	 */
	public function configPath()
	{
		return $this->basePath.DIRECTORY_SEPARATOR.'config';
	}

	/**
	 * Get the path to the database directory.
	 *
	 * @return string
	 */
	public function databasePath()
	{
		return $this->databasePath ?: $this->basePath.DIRECTORY_SEPARATOR.'database';
	}

	/**
	 * Set the base path for the application.
	 *
	 * @param  string  $basePath
	 * @return $this
	 */
	public function setBasePath($basePath)
	{
		$this->basePath = $basePath;

		return $this;
	}

	/**
	 * Get or check the current application environment.
	 *
	 * @param  mixed
	 * @return string
	 */
	public function environment()
	{
		return getenv('APP_ENV');
	}
}