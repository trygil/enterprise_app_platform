<?php namespace EAP\Migration;

use Illuminate\Database\Migrations\Migration as BaseMigration;
use Illuminate\Database\Capsule\Manager as Capsule;

class Migration extends BaseMigration
{
    protected $schema;

    public function __construct()
    {
        try {
            // \Illuminate\Support\Facades\Schema::create();
        }
        catch (Exception $e) {
            
        } 
        finally {
            $this->schema = Capsule::schema(); 
        }
    }
}